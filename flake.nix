{
    description = "xmrig";

    inputs = {
        nixpkgs.url = github:NixOS/nixpkgs;
        flake-utils.url = "github:numtide/flake-utils";
    };

    outputs = { self, flake-utils, nixpkgs }:
        flake-utils.lib.eachSystem ["x86_64-linux" "x86_64-darwin"] (system:
            let
                pkgs = import nixpkgs { inherit system; };
            in {
                packages.xmrig = with pkgs; with pkgs.lib; stdenv.mkDerivation rec {
                    pname = "xmrig";
                    version = "6.13.1";

                    src = fetchFromGitHub {
                        owner = "xmrig";
                        repo = "xmrig";
                        rev = "v${version}";
                        sha256 = "ocGIMJV/AZC2Qd44QLwbOUHRPljc6y/h1kLpxUxFdJg=";
                    };

                    nativeBuildInputs = [ cmake ];
                    buildInputs = [ libuv libmicrohttpd openssl hwloc ];

                    postPatch = ''
                        substituteInPlace src/donate.h \
                        --replace "kDefaultDonateLevel = 5;" "kDefaultDonateLevel = 0;" \
                        --replace "kMinimumDonateLevel = 1;" "kMinimumDonateLevel = 0;"
                    '';

                    installPhase = ''
                        install -vD xmrig $out/bin/xmrig
                    '';

                    meta = with lib; {
                        description = "Monero (XMR) CPU miner";
                        homepage = "https://github.com/xmrig/xmrig";
                        license = licenses.gpl3Plus;
                    };
                };
            }
        );
}
